export default (context) => {
    // 쿠키의 토큰을 가지고 있지 않으면 로그인 페이지로 보내버림.
    if (!context.app.context.app.$cookies.get('token')) {
        return context.redirect('/login')
    }
}

// 권한 갖고 있는지 아닌지 체크하는거
