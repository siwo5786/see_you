export default (context) => {
    if (context.app.context.app.$cookies.get('token')) {
        return context.redirect('/')
    }
}
// 익명인지 아닌지 체크하는거 .
