import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store) => {
        return axios.get(apiUrls.DO_LIST)
    },
    [Constants.DO_LIST_PAGING]: (store, payload) => {
        let paramArray = []
        if (payload.params.searchName.length >= 1) paramArray.push(`searchName=${payload.params.searchName}`)
        if (payload.params.searchAge != null) paramArray.push(`searchAge=${payload.params.searchAge}`)
        if (payload.params.searchYear != null) paramArray.push(`searchYear=${payload.params.searchYear}`)
        if (payload.params.searchMonth != null) paramArray.push(`searchMonth=${payload.params.searchMonth}`)
        let paramText = paramArray.join('&')

        return axios.get(apiUrls.DO_LIST_PAGING.replace('{pageNum}', payload.pageNum) + '?' + paramText)
    },
    [Constants.DO_REFUND]: (store, payload) => {
        return axios.put(apiUrls.DO_REFUND, payload)
    },
    [Constants.DO_DATA]: (store, payload) => {
        return axios.post(apiUrls.DO_DATA, payload)
    },
    [Constants.DO_FILE_UPLOAD]: (store, payload) => {
        let data= new FormData()
        data.append('csvFile', payload)

        let config = {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }
        return axios.post(apiUrls.DO_FILE_UPLOAD, data, config)
    },
}
