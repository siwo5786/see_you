import state from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'

export default {
    state, mutations, actions, getters
}
// 그 폴더의 것들을 모아주는 용
