import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_DATA]: (store, payload) => {
        return axios.post(apiUrls.DO_DATA.replace('{productId}', payload.productId), payload.list)
    },
    [Constants.DO_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_DELETE.replace('{id}', payload.id))
    },
    [Constants.DO_LIST]: (store, payload) => {
        return axios.get(apiUrls.DO_LIST.replace('{page}', payload.page))
    },
    [Constants.DO_LIST_DETAIL]: (store, payload) => {
        let paramText = `id=${payload.id}`
        return axios.get(apiUrls.DO_LIST_DETAIL + '?' + paramText)
    },
    [Constants.DO_LIST_LACK]: (store, payload) => {
        return axios.get(apiUrls.DO_LIST_LACK.replace('{page}', payload.page))
    },
    [Constants.DO_UPDATE]: (store, payload) => {
        return axios.put(apiUrls.DO_UPDATE.replace('{id}', payload.id), payload.list)
    },
}
