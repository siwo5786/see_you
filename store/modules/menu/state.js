const state = () => ({
    globalMenu: [
        {
            parentName: 'HOME',
            menuLabel: [
                { id: 'DASH_BOARD', icon: 'el-icon-tickets', currentName: '대시보드', link: '/', isShow: true },
            ]
        },
        {
            parentName: '회원관리',
            menuLabel: [
                { id: 'MEMBER_ADD', icon: 'el-icon-tickets', currentName: '회원등록', link: '/', isShow: true },
                { id: 'MEMBER_EDIT', icon: 'el-icon-tickets', currentName: '회원수정', link: '/', isShow: true },
                { id: 'MEMBER_LIST', icon: 'el-icon-tickets', currentName: '회원리스트', link: '/customer/list', isShow: true },
                { id: 'MEMBER_DETAIL', icon: 'el-icon-tickets', currentName: '회원상세정보', link: '/', isShow: true },
            ]
        },
        {
            // 내가 한거
            parentName: '상품 관리',
            menuLabel: [
                { id: 'PRODUCT_ADD', icon: 'el-icon-tickets', currentName: '상품 등록', link: '/', isShow: true },
                { id: 'PRODUCT_LIST', icon: 'el-icon-tickets', currentName: '상품 리스트', link: '/', isShow: true },
            ]
        },
        {
            // 내가 한거
            parentName: '판매 관리',
            menuLabel: [
                { id: 'SELL_ADD', icon: 'el-icon-tickets', currentName: '판매 등록', link: '/sell/form', isShow: true },
                { id: 'SELL_LIST', icon: 'el-icon-tickets', currentName: '판매 리스트', link: '/sell/list', isShow: true },
                { id: 'SELL_REFUND', icon: 'el-icon-tickets', currentName: '환불', link: '/sell/refund', isShow: true },
            ]
        },
        {
            parentName: '상품관리',
            menuLabel: [
                { id: 'PRODUCT_ADD', icon: 'el-icon-tickets', currentName: '상품등록', link: '/product/form', isShow: true },
                { id: 'PRODUCT_EDIT', icon: 'el-icon-tickets', currentName: '상품수정', link: '/product/refund', isShow: true },
                { id: 'PRODUCT_LIST', icon: 'el-icon-tickets', currentName: '상품리스트', link: '/product/list', isShow: true },
            ]
        },
        {
            // 내가 한거
            parentName: '재고 관리',
            menuLabel: [
                { id: 'STOCK_ADD', icon: 'el-icon-tickets', currentName: '재고 등록', link: '/stock/form', isShow: true },
                { id: 'STOCK_LIST', icon: 'el-icon-tickets', currentName: '재고 리스트', link: '/stock/list', isShow: true },
                { id: 'STOCK_LIST_LACK', icon: 'el-icon-tickets', currentName: '재고 부족 리스트', link: '/stock/list-lack', isShow: true },
            ]
        },
        {
            // 내가 한거
            parentName: '게시판 관리',
            menuLabel: [
                { id: 'BOARD_ADD', icon: 'el-icon-tickets', currentName: '게시글 등록', link: '/board/form', isShow: true },
                { id: 'BOARD_LIST', icon: 'el-icon-tickets', currentName: '게시글 리스트', link: '/board/list', isShow: true },
                { id: 'BOARD_EDIT', icon: 'el-icon-tickets', currentName: '게시글 수정', link: '/board/edit', isShow: true },
            ]
        },
        {
            parentName: '마이 메뉴',
            menuLabel: [
                { id: 'MEMBER_LOGOUT', icon: 'el-icon-lock', currentName: '로그아웃', link: '/my-menu/logout', isShow: true },
            ]
        },
    ],
    selectedMenu: 'DASH_BOARD'
})

export default state
