import customLoading from './modules/custom-loading'
import authenticated from './modules/authenticated'
import menu from './modules/menu'
import testData from './modules/test-data'
import board from './modules/board'
import stock from './modules/stock'
import sell from './modules/sell'
import product from './modules/product'
export const state = () => ({})

export const mutations = {}

export const modules = {
    customLoading,
    authenticated,
    menu,
    testData,
    board,
    stock,
    sell,
    product
}
