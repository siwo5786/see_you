import Vue from 'vue'

import customLoadingConstants from '~/store/modules/custom-loading/constants'
Vue.prototype.$customLoadingConstants = customLoadingConstants

import authenticatedConstants from '~/store/modules/authenticated/constants'
Vue.prototype.$authenticatedConstants = authenticatedConstants

import menuConstants from '~/store/modules/menu/constants'
Vue.prototype.$menuConstants = menuConstants

import testDataConstants from '~/store/modules/test-data/constants'
Vue.prototype.$testDataConstants = testDataConstants

import boardConstants from '~/store/modules/board/constants'
Vue.prototype.$boardConstants = boardConstants

import stockConstants from '~/store/modules/stock/constants'
Vue.prototype.$stockConstants = stockConstants

import sellConstants from '~/store/modules/sell/constants'
Vue.prototype.$sellConstants = sellConstants

import productConstants from '~/store/modules/product/constants'
Vue.prototype.$productConstants = productConstants




